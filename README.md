## Deployment Project in your local machine

1. clone this project - git clone git@gitlab.com:elchibek00/weather-api.git
2. create .env file (copy .env.example)
3. composer install
4. php artisan key:generate
5. add Your Weather API KEY in .env file (OPEN_WEATHER_API_KEY)
6. php artisan serve
7. you can send from postman on http://127.0.0.1:8000/api/weather/ (you can add city, lang code and unit)

