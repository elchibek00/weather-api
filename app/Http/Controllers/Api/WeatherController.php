<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\WeatherRequest;

class WeatherController extends Controller
{
    protected string $city = 'Bishkek';
    protected string $unit = 'metric';
    protected string $lang = 'en';

    public function __invoke(WeatherRequest $request)
    {
        $api_key = config('app.weather_api_key');

        $validatedData = $request->validated();

        if(!empty($validatedData['city'])){
            $this->city = ucfirst(strtolower($validatedData['city']));
        }
        if(!empty($validatedData['unit'])){
            $this->unit = strtolower($validatedData['unit']);
        }
        if(!empty($validatedData['lang'])){
            $this->lang = strtolower($validatedData['lang']);
        }

        $response = Http::get(
            "https://api.openweathermap.org/data/2.5/weather?q={$this->city}&appid={$api_key}&units={$this->unit}&lang={$this->lang}"
        );

        if($response->successful()){
            return $response->json();
        }

        return response()
            ->json(['message' => 'Please check your request and try again!',
                    'valid units' => ['standard', 'metric', 'imperial', ],
                    'valid languages code' =>
                    ['af', 'al', 'ar', 'az', 'bg', 'ca', 'cz', 'da', 'de', 'el',
                     'en', 'eu', 'fa', 'fi', 'fr', 'gl', 'he', 'hi', 'hr', 'hu', 'id',
                     'it', 'ja', 'kr', 'la', 'lt', 'mk', 'no', 'nl', 'pl', 'pt', 'pt_br',
                     'ro', 'ru', 'sv', 'se', 'sk', 'sl', 'sp', 'es', 'sr', 'th', 'tr',
                     'ua', 'uk', 'vi', 'zh_cn', 'zh_tw', 'zu',
                    ]
                    ]);
    }
}
